package mhsquire.com.movesmonster1.logic;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import mhsquire.com.movesmonster1.models.Armor;
import mhsquire.com.movesmonster1.models.Model;

public class CommentsDataSource {


    // Database fields
    private SQLiteDatabase database;
    private SQLDatabaseHelper dbHelper;
    private String TABLE_TITLE;
    private String[] allColumns = {SQLDatabaseHelper.COLUMN_ID,
            TABLE_TITLE};
    public Model __model= new Model();
    public CommentsDataSource(Context context, Model model) {
        dbHelper = new SQLDatabaseHelper(context);
        TABLE_TITLE = model.TITLE;
        __model = model;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public List<__model> getAllComments() {
        List<Armor> comments = new ArrayList<Armor>();

        Cursor cursor = database.query(SQLDatabaseHelper.TABLES[0],
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Armor armor = cursorToArmor(cursor);
            comments.add(armor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comments;
    }

    private Armor cursorToArmor(Cursor cursor) {
        Armor armor = new Armor();
        armor.setId(cursor.getLong(0));
        armor.setComment(cursor.getString(1));
        return armor;
    }
}
