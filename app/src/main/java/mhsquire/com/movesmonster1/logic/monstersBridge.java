package mhsquire.com.movesmonster1.logic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import mhsquire.com.movesmonster1.models.Monsters;

/**
 * Created by owner on 10/16/2015.
 */
public class monstersBridge {

    // Database fields
    private SQLiteDatabase database;
    private SQLDatabaseHelper dbHelper;
    private String[] allColumns = { SQLDatabaseHelper.COLUMN_ID,
            SQLDatabaseHelper.COLUMN_TEXT };


    public monstersBridge(Context context) {
        dbHelper = new SQLDatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Monsters createMonster(String monster) {
        ContentValues values = new ContentValues();
        values.put(SQLDatabaseHelper.COLUMN_TEXT, monster);
        long insertId = database.insert(SQLDatabaseHelper.TABLES[0], null,
                values);
        Cursor cursor = database.query(SQLDatabaseHelper.TABLES[0],
                allColumns, SQLDatabaseHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Monsters newMonster = cursorToMonster(cursor);
        cursor.close();
        return newMonster;
    }

    public void deleteMonster(Monsters monster) {
        long id = monster.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(SQLDatabaseHelper.TABLES[0], SQLDatabaseHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Monsters> getAllComments() {
        List<Monsters> comments = new ArrayList<Monsters>();

        Cursor cursor = database.query(SQLDatabaseHelper.TABLES[0],
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Monsters monster = cursorToMonster(cursor);
            comments.add(monster);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comments;
    }

    private Monsters cursorToMonster(Cursor cursor) {
        Monsters monster = new Monsters();
        monster.setId(cursor.getLong(0));
        monster.setComment(cursor.getString(1));
        return monster;
    }
}
