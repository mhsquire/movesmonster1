package mhsquire.com.movesmonster1.handlers;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import mhsquire.com.movesmonster1.Constants;
import mhsquire.com.movesmonster1.R;
import mhsquire.com.movesmonster1.logic.StepDetect;
import mhsquire.com.movesmonster1.logic.StepDisplayer;

/**
 * Created by owner on 2/23/2015.
 */
public class PedometerService extends Service {
    private static final String TAG = "PedometerService";

    private SensorManager mSensorManager;
    private StepDetect pedometer;
    private StepDisplayer mStepDisplayer;
    private int mSteps;
    private SharedPreferences gameData;

    Context context = null;
    private boolean safeStop = false;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "[SERVICE] onStart");
        super.onStartCommand(intent, flags, startId);
        startBackgroundTask(intent, startId);
        // Tell the user we started.
        Toast.makeText(this, getText(R.string.started), Toast.LENGTH_SHORT).show();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "[SERVICE] onDestroy");
        unregisterDetector();
        // Stop detecting
        mSensorManager.unregisterListener(pedometer);

        // Tell the user we stopped.
        Toast.makeText(this, getText(R.string.stopped), Toast.LENGTH_SHORT).show();
        super.onDestroy();


    }

    private void registerDetector() {
        Sensor mSensor = mSensorManager.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER /*|
            Sensor.TYPE_MAGNETIC_FIELD |
            Sensor.TYPE_ORIENTATION*/);
        mSensorManager.registerListener(pedometer,
                mSensor,
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void unregisterDetector() {
        mSensorManager.unregisterListener(pedometer);
    }
    /**
     * Forwards pace values from PaceNotifier to the activity.
     */
    private StepDisplayer.Listener mStepListener = new StepDisplayer.Listener() {
        public void stepsChanged(int value) {
            mSteps = value;
            passValue();
        }
        public void passValue() {
            if (mCallback != null) {
                mCallback.stepsChanged(mSteps);
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class MyBinder extends Binder {
        PedometerService getService() {
            return PedometerService.this;
        }
    }
    private final IBinder binder = new MyBinder();

    public interface ICallback {
        public void stepsChanged(int value);
    }

    private ICallback mCallback;
/*
    public void registerCallback(ICallback cb) {
        mCallback = cb;
        mStepDisplayer.passValue();
        //mPaceListener.passValue();
    }
*/

    public void setSafeStop() {
        safeStop = true;
    }

    private void  startBackgroundTask(Intent intent, int startId) {
        // Start a background thread and begin the processing.
        backgroundExecution();
    }

    //This method is called on the main GUI thread.
    private void backgroundExecution() {
        // This moves the time consuming operation to a child thread.
        Thread thread = new Thread(null, doBackgroundThreadProcessing,
                "Background");
        thread.start();
    }

    //Runnable that executes the background processing method.
    private final Runnable doBackgroundThreadProcessing = new Runnable() {
        public void run() {
            backgroundThreadProcessing();
        }
    };

    //Method which does some processing in the background.
    private void backgroundThreadProcessing() {
        // [ ... Time consuming operations ... ]
        int previous = 0;
        //pedometer.Start(context);

        //Hopefully passes on Services context
        context = super.getApplicationContext();
        pedometer = new StepDetect();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        registerDetector();

        gameData = context.getSharedPreferences("state", Context.MODE_PRIVATE);

        mStepDisplayer = new StepDisplayer();
        mStepDisplayer.setSteps(mSteps = gameData.getInt("steps", 0));
        mStepDisplayer.addListener(mStepListener);
        pedometer.addStepListener(mStepDisplayer);


        while (!safeStop) {
                if(mSteps != previous) {
                    Log.i(TAG, "steps is " + mSteps);
                    previous = mSteps;
                    Constants.steps = mSteps;
                    Constants.stepsSinceTaskStart ++;
                    Log.i(TAG, "steps is " + Constants.stepsSinceTaskStart);
                }
        }

        SharedPreferences.Editor mStateEditor = gameData.edit();
        mStateEditor.putInt("steps", mSteps);
        mStateEditor.apply();
        Log.i(TAG, "Service Complete");
        stopSelf();
    }
}
