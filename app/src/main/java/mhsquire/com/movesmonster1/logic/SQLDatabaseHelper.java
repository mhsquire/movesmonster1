package mhsquire.com.movesmonster1.logic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by owner on 10/16/2015.
 */
public class SQLDatabaseHelper extends SQLiteOpenHelper{

    public static final String[] TABLES = {"Armor", "Weapon", "Monsters", "Player"};

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TEXT = "text";

    private static final String DATABASE_NAME = "movesmonster1.db";
    private static final int DATABASE_VERSION = 1;




    public SQLDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String DATABASE_CREATE;
        for ( String TABLE : TABLES){
            // Database creation sql statement
            DATABASE_CREATE = "create table "
                    + TABLE + "(" + COLUMN_ID
                    + " integer primary key autoincrement, " + COLUMN_TEXT
                    + " text not null);";
            db.execSQL(DATABASE_CREATE);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLES);
        onCreate(db);
    }

}



