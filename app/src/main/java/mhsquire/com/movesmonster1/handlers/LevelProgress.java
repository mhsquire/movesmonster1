package mhsquire.com.movesmonster1.handlers;

import mhsquire.com.movesmonster1.Constants;
import mhsquire.com.movesmonster1.logic.StepListener;

/**
 * Created by owner on 3/20/2015.
 */
public class LevelProgress implements StepListener {

    double currLvlInterval;
    double lvlExpProgress;
    int mProgressStatus;

    public LevelProgress() {
        this.currLvlInterval = currLvlInterval;
        this.lvlExpProgress = lvlExpProgress;
    }

    public void update() {
        mProgressStatus = (int) Math.floor((lvlExpProgress / currLvlInterval) * 100);
    }

    @Override
    public void onStep() {
        Constants.stepsSinceTaskStart = Constants.stepsSinceTaskStart + 1;
    }

    @Override
    public void passValue() {

    }
}
