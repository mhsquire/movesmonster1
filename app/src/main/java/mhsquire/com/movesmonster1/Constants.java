package mhsquire.com.movesmonster1;

/**
 * Created by owner on 3/20/2015.
 */
public class Constants {

    public static int mLevelProgressStatus = 0;
    public static int mActivityProgressStatus = 0;
    public static boolean safeStop = false;
    public static int mProgressStatus = 0;
    public static int level = 1;
    public static int steps = 0;
    public static int mDaily = 0;
    public static int mOpponent = 0;
    public static int mSword = 0;
    public static int mShield = 0;
    public static int mStepsForTaskCompletion = 5;
    public static int stepsSinceTaskStart = 0;

    public static final int indexOfFighting = 2;
    public static final int numberofopponents = (10) -1;
    public static final int stepDifficulty = 100;
}
