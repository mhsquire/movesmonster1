package mhsquire.com.movesmonster1.views.widgets;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import mhsquire.com.movesmonster1.Constants;
import mhsquire.com.movesmonster1.R;

/**
 * Created by owner on 3/21/2015.
 */
public class ActivityBar extends ProgressBar {
    private ProgressBar activityProgress;
    private Handler mHandler = new Handler();
    public ActivityBar(Context context) {
        super(context);
        activityProgress = (ProgressBar) findViewById(R.id.activityBar);
    }

    public ActivityBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        activityProgress = (ProgressBar) findViewById(R.id.activityBar);
    }

    public ActivityBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        activityProgress = (ProgressBar) findViewById(R.id.activityBar);
    }


    public void startActivityBar() {
        new Thread(new Runnable() {
            public void run() {

                while (!Constants.safeStop) {

                    while (Constants.mLevelProgressStatus < 100 && !Constants.safeStop) {
                        // Update the progress bar with last value
                        Constants.mActivityProgressStatus =
                                100 * (int) Math.floor(Constants.stepsSinceTaskStart /
                                        Constants.mStepsForTaskCompletion);
                        mHandler.post(new Runnable() {
                            public void run() {
                                activityProgress.setProgress(Constants.mActivityProgressStatus);
                            }
                        });
                    }

                    mHandler.post(new Runnable() {
                        public void run() {
                            //activitytitle.setText(sayWhatHappened());
                            }
                    });
                    Constants.mActivityProgressStatus = 0;

                }
            }
        }).start();
    }
}
