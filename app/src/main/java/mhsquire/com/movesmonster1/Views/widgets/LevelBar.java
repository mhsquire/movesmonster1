package mhsquire.com.movesmonster1.views.widgets;

import android.content.Context;
import android.os.Handler;
import android.widget.ProgressBar;

import mhsquire.com.movesmonster1.Constants;

/**
 * Created by owner on 3/21/2015.
 */
public class LevelBar extends ProgressBar {

    private Handler mHandler = new Handler();

    public LevelBar(Context context) {
        super(context);
        Constants.level = currentLevel(Constants.steps);
    }
    private void startLevelBar() {
        new Thread(new Runnable() {
            public void run() {
                double currLvlInterval = currLvlInterval();
                double lvlExpProgress = lvlExpProgress();
                Constants.mLevelProgressStatus = (int)  Math.floor((lvlExpProgress / currLvlInterval) * 100);
                while (!Constants.safeStop) {
                    while (Constants.mLevelProgressStatus < 100 && !Constants.safeStop) {
                        // Update the progress bar with last value
                        Constants.mLevelProgressStatus = (int) Math.floor((lvlExpProgress() / currLvlInterval()) * 100);
                        mHandler.post(new Runnable() {
                            public void run() {
                                //levelProgress.setProgress(Constants.mLevelProgressStatus);
                            }
                        });
                    }
                    Constants.level = currentLevel(Constants.steps);
                    mHandler.post(new Runnable() {
                        public void run() {
                            //leveltitle.setText("Level " + Constants.level);
                            }
                    });
                    Constants.mLevelProgressStatus = 0;
                }
            }
        }).start();
    }

    private int currentLevel(int steps) {
        return (int) Math.floor(Math.sqrt(Constants.steps/ Constants.stepDifficulty)) ;
    }

    private double currLvlInterval() {
        double nextLevel = Constants.level + 1;
        return (Constants.stepDifficulty * Math.pow(nextLevel, 2) -  Constants.stepDifficulty * Math.pow(Constants.level, 2));
    }

    private double lvlExpProgress() {
        return  Constants.steps - (Constants.stepDifficulty * Math.pow(Constants.level, 2));
    }

}
