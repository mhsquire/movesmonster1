package mhsquire.com.movesmonster1.views;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import mhsquire.com.movesmonster1.Constants;
import mhsquire.com.movesmonster1.handlers.PedometerService;
import mhsquire.com.movesmonster1.R;
import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends FragmentActivity {
    private static final int stepDifficulty = 100;
    private static String TAG = "MainActivity";
    private static final String PrefName = "state";
    private Handler mHandler = new Handler();
    private Handler mActivityHandler = new Handler();
    //static final String Tag = "Main Activity";
    private ProgressBar levelProgress;
    private ProgressBar activityProgress;
    //private int mProgressStatus = 0;
    private SharedPreferences gameData = null;
    private SharedPreferences.Editor editor = null;
    boolean safeStop = false;
    @InjectView (R.id.textView3) TextView leveltitle;
    @InjectView (R.id.engaged_activity) TextView activitytitle;

    private String[] dailies = null;
    private String [] opponents = null;
    private String[] sword = null;
    private String [] shield = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getFragmentManager();
        gameData = getSharedPreferences(PrefName, Context.MODE_PRIVATE);
        editor = gameData.edit();
        Constants.steps = gameData.getInt("steps", 0);
        Constants.level = currentLevel(Constants.steps);
        editor.putInt("level", Constants.level);
        editor.commit();

        levelProgress = (ProgressBar) findViewById(R.id.levelBar);
        activityProgress = (ProgressBar) findViewById(R.id.activityBar);
        ButterKnife.inject(this);
        Resources res = getResources();
        dailies = res.getStringArray(R.array.daily_event);
        opponents = res.getStringArray(R.array.opponent);
        sword = res.getStringArray(R.array.sword);
        shield = res.getStringArray(R.array.shield);

        leveltitle.setText("Level " + Constants.level);
        // Check to see if the Fragment back stack has been populated
        // If not, create and populate the layout.

        HistoryFragment historyFragment =
                (HistoryFragment)fm.findFragmentById(R.id.fragment_container);

        if (historyFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragment_container, new HistoryFragment());
            // ft.add(R.id.ui_container, new MyListFragment());
            ft.commit();
        }

        //launches the PedometerService intent
        //todo look into using intents to pass information
        Intent intent = new Intent(this, PedometerService.class);
        startService(intent);

        //launches the levelBar thread used to display progress toward next level
        startLevelBar();

        //launches the activityBar thread used to display progress toward next Activity
        startActivityBar();



        bindToService();
    }

    private void startActivityBar() {
        new Thread(new Runnable() {
            public void run() {
                int previousStatus = 0;
                Constants.mActivityProgressStatus = ((int) (((double) Constants.stepsSinceTaskStart / (double) Constants.mStepsForTaskCompletion) * 100));
                while (!safeStop) {
                    while (Constants.mActivityProgressStatus < 100 && !safeStop) {
                        // Update the progress bar with last value
                        Constants.mActivityProgressStatus = ((int) (((double) Constants.stepsSinceTaskStart / (double) Constants.mStepsForTaskCompletion) * 100));
                        if(Constants.mActivityProgressStatus != previousStatus) {
                            mActivityHandler.post(new Runnable() {
                                public void run() {
                                    activityProgress.setProgress(Constants.mActivityProgressStatus);
                                }
                            });
                            previousStatus = Constants.mActivityProgressStatus;
                        }
                    }
                    mActivityHandler.post(new Runnable() {
                        public void run() {
                            activitytitle.setText("" + saySomething());
                        }
                    });
                    Constants.stepsSinceTaskStart = 0;
                    Constants.mActivityProgressStatus = 0;
                }
            }
        }).start();
    }


    private String saySomething() {
        Constants.mDaily = Constants.mDaily + 1;
        if(Constants.mDaily >= dailies.length) {
            Constants.mDaily = 0;
        }
        if(Constants.mDaily == Constants.indexOfFighting) {
            return "" + dailies[Constants.mDaily] + " " + opponents[(int) (Math.random() * Constants.numberofopponents)];
        } else {
            return "" + dailies[Constants.mDaily];
        }
    }



    private void startLevelBar() {
        new Thread(new Runnable() {
            public void run() {
                int previousStatus = 0;
                mHandler.post(new Runnable() {
                    public void run() {
                        leveltitle.setText("Level " + Constants.level);                        }
                });
                double currLvlInterval = currLvlInterval();
                double lvlExpProgress = lvlExpProgress();
                Constants.mProgressStatus = (int)  Math.floor((lvlExpProgress
                                                            / currLvlInterval) * 100);
                while (!safeStop) {
                    while (Constants.mProgressStatus < 100 && !safeStop) {
                        // Update the progress bar with last value
                        Constants.mProgressStatus = (int) Math.floor((lvlExpProgress()
                                                                / currLvlInterval()) * 100);

                        if(Constants.mProgressStatus != previousStatus) {
                            mHandler.post(new Runnable() {
                                public void run() {
                                    levelProgress.setProgress(Constants.mProgressStatus);
                                }
                            });
                            previousStatus = Constants.mProgressStatus;
                        }
                    }
                    Constants.level = currentLevel(Constants.steps);
                    mHandler.post(new Runnable() {
                        public void run() {
                            leveltitle.setText("Level " + Constants.level);                        }
                    });
                    Constants.mProgressStatus = 0;
                }
            }
        }).start();
    }

    private int currentLevel(int steps) {
        return (int) Math.floor(Math.sqrt(Constants.steps/stepDifficulty)) ;
    }

    private double currLvlInterval() {
        double nextLevel = Constants.level + 1;
        return (stepDifficulty * Math.pow(nextLevel, 2) -  stepDifficulty * Math.pow(Constants.level, 2));
    }

    private double lvlExpProgress() {
        return  Constants.steps - (stepDifficulty * Math.pow(Constants.level, 2));
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        editor.putInt("steps", Constants.steps);
        editor.putInt("level", Constants.level);
        editor.putInt("activityProgress", Constants.stepsSinceTaskStart);
        editor.commit();
        serviceRef.setSafeStop();
        unbindFromService();
        safeStop = true;
    }


    boolean getStop() {
        boolean safeStop = false;
        return safeStop;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Creates a connection to the running service

    //Reference to the service
    private PedometerService serviceRef;
/*
    // Handles the connection between the service and activity
    public final ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName classname, IBinder service) {
            // Called when connection is made.
            serviceRef = ((PedometerService.MyBinder)service).getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            // Received when the service unexpectedly disconnects.
            serviceRef = null;
        }
    };
*/
    private void bindToService() {
        //Bind to the service
        Intent bindIntent = new Intent(MainActivity.this, PedometerService.class);
        bindService(bindIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindFromService() {
        //Unbind from the service
        //Intent unbindIntent = new Intent(MainActivity.this, PedometerService.class);
        unbindService(mConnection);
    }


}
