package mhsquire.com.movesmonster1.models;

/**
 * Created by owner on 10/17/2015.
 */

import mhsquire.com.movesmonster1.models.*;

public class Model {

    private long id;
    private String comment;
    public String TITLE;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return comment;
    }
}
