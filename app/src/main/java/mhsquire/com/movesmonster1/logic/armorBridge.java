package mhsquire.com.movesmonster1.logic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import mhsquire.com.movesmonster1.models.Armor;

/**
 * Created by owner on 10/16/2015.
 */
public class armorBridge {

    // Database fields
    private SQLiteDatabase database;
    private SQLDatabaseHelper dbHelper;
    private String[] allColumns = { SQLDatabaseHelper.COLUMN_ID,
            SQLDatabaseHelper.COLUMN_TEXT };


    public armorBridge(Context context) {
        dbHelper = new SQLDatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Armor createArmor(String armor) {
        ContentValues values = new ContentValues();
        values.put(SQLDatabaseHelper.COLUMN_TEXT, armor);
        long insertId = database.insert(SQLDatabaseHelper.TABLES[0], null,
                values);
        Cursor cursor = database.query(SQLDatabaseHelper.TABLES[0],
                allColumns, SQLDatabaseHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Armor newArmor = cursorToArmor(cursor);
        cursor.close();
        return newArmor;
    }

    public void deleteArmor(Armor armor) {
        long id = armor.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(SQLDatabaseHelper.TABLES[0], SQLDatabaseHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Armor> getAllComments() {
        List<Armor> comments = new ArrayList<Armor>();

        Cursor cursor = database.query(SQLDatabaseHelper.TABLES[0],
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Armor armor = cursorToArmor(cursor);
            comments.add(armor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comments;
    }

    private Armor cursorToArmor(Cursor cursor) {
        Armor armor = new Armor();
        armor.setId(cursor.getLong(0));
        armor.setComment(cursor.getString(1));
        return armor;
    }
}
